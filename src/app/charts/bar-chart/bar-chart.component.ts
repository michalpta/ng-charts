import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {

  @Input() set data(value: any) {
    this.options = this.getOptions(value);
  };
  @Output() pointClick = new EventEmitter<any>();

  @Input() update: boolean;
  @Output() updateChange = new EventEmitter<boolean>();

  highcharts = Highcharts;

  options: any;

  constructor() { }

  ngOnInit(): void { }

  getOptions = (data) => ({
    title: null,
    chart: {
      spacing: [0, 0, 0, 0],
      styledMode: false
    },
    tooltip: {
      enabled: false
    },
    plotOptions: {
      series: {
          borderWidth: 0,
          borderRadius: 0,
          stacking: 'normal',
          pointWidth: 15,
          pointPadding: 0,
          groupPadding: 0,
          cursor: 'pointer',
          point: {
            events: {
                click: (e) => this.pointClick.emit(e.point)
            }
          }
        }
    },
    xAxis: {
      labels: 'disabled',
      visible: false,
      categories: ['Status']
    },
    yAxis: {
      labels: 'disabled',
      visible: false,
      min: 0,
      max: 100,
      title: {
        text: null
      }
    },
    legend: {
      margin: 5,
      enabled: false
    },
    series: data,
    credits: {
      enabled: false
    }
  });

}
