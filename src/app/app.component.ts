import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit { 

  value1 = 75;
  value2 = 15;
  value3 = 10;

  chartData = [{
    name: 'Not started',
    data: [{ y: this.value3 }],
    type: 'bar',
    color: 'lightgray'
  },{
    name: 'In progress',
    data: [{ y: this.value2 }],
    type: 'bar',
    color: '#ffc107'
  },{
    name: 'Completed',
    data: [{ y: this.value1 }],
    type: 'bar',
    color: '#28a745'
  }];

  updateFlag = false;

  message: string;

  ngOnInit() { }  

  handleClick(point) {
    console.log(point);
    this.message = `${point.series.name} count: ${point.y}!`;
  }
}
